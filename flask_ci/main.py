from flask import Flask
from flask_cors import CORS
from flask import request
from pprint import pprint as pp
import json

app = Flask('app')
CORS(app)

@app.route('/')
@app.route('/<name>')
def hello(name="flask"):
    return f"Hello, {name}!"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)