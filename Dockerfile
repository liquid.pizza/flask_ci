# set base image (host OS)
FROM python:3.10

# set the working directory in the container
WORKDIR /src

# install dependencies
RUN pip install poetry

# copy the content of the local src directory to the working directory
COPY ./ .

# install dependencies
RUN poetry install

# command to run on container start
ENTRYPOINT [ "poetry", "run", "python", "flask_ci/main.py" ] 